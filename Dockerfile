FROM centos/systemd

MAINTAINER DucCNV chauduc.1211@gmail.com

#Install repository
RUN yum -y update
RUN yum -y install epel-release
RUN yum -y localinstall http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
RUN yum -y localinstall http://download.postgresql.org/pub/repos/yum/9.3/redhat/rhel-7-x86_64/pgdg-centos93-9.3-2.noarch.rpm

#Add user: interdev
RUN useradd -m interdev && usermod -aG wheel interdev

#Add initscripts
RUN yum -y install initscripts && yum clean all

#Add C/C++ install
RUN yum install -y gcc

#Add wget command
RUN yum install -y wget

#Add PSQL & contrib
RUN yum install -y postgresql93 postgresql93-server postgresql93-contrib postgresql93-libs

#Start psql service
RUN systemctl enable postgresql-9.3

RUN /usr/pgsql-9.3/bin/postgresql93-setup initdb

#Open port 
EXPOSE 5432

CMD ["/usr/sbin/init"]
